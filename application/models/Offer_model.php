<?php

Class Offer_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("offers");

    }

    public function getOfferProductsSubCategories($offer, $language_code)
    {

        $offer = json_decode($offer);

        $this->db->select('products.SubCategoryID,categories.CategoryID,categories_text.Title');
        $this->db->from('products');
        $this->db->join('categories', 'categories.CategoryID = products.SubCategoryID');
        $this->db->join('categories_text', 'categories_text.CategoryID = categories.CategoryID');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = categories_text.SystemLanguageID');
        $this->db->where('system_languages.ShortCode', $language_code);
        $products = explode(',', $offer->ProductID);
        if (COUNT($products)) {
            $this->db->where_in('products.ProductID', $products);

        } else {
            $this->db->where('products.ProductID', $offer->ProductID);

        }


        return $this->db->get()->result();


    }


    public function getOfferProducts($offer)
    {


        // $this->db->select('offers.ProductID');
        $this->db->select('offers.*,offers_text.*');
        $this->db->from('offers');
        $this->db->join('offers_text', 'offers.OfferID = offers_text.OfferID');

        if (COUNT($offer) > 1) {
            $this->db->where_in('offers.OfferID', $offer);

        } else {
            $this->db->where('offers.OfferID', $offer[0]);

        }


        return $this->db->get()->result();


    }


     public function check_product_in_offer($product_id)
    {


        // $this->db->select('offers.ProductID');
        $this->db->select('offers.*,offers_text.*');
        $this->db->from('offers');
        $this->db->join('offers_text', 'offers.OfferID = offers_text.OfferID');

        if (COUNT($offer) > 1) {
            $this->db->where_in('offers.OfferID', $offer);

        } else {
            $this->db->where('offers.OfferID', $offer[0]);

        }


        return $this->db->get()->result();


    }

    public function getOfferForUser($user_id, $language_code = 'EN')
    {


        $this->db->select('offers.*,offers_text.*');
        $this->db->from('offers');
        $this->db->join('offers_text', 'offers.OfferID = offers_text.OfferID');

        $this->db->join('offers_groups', 'offers.OfferID = offers_groups.OfferID');
        $this->db->join('customer_groups', 'offers_groups.GroupID = customer_groups.CustomerGroupID');
        $this->db->join('customer_group_members', 'customer_groups.CustomerGroupID = customer_group_members.CustomerGroupID');


        $this->db->join('system_languages', 'system_languages.SystemLanguageID = offers_text.SystemLanguageID');
        $this->db->where('system_languages.ShortCode', $language_code);


        $this->db->where('customer_group_members.UserID', $user_id);
        $this->db->where('offers.IsForAll', 0);
        $this->db->where('offers.IsActive', 1);
        $this->db->where('Date(offers.ValidTo) >', date('Y-m-d'));

        $this->db->group_by('offers.OfferID');

        return $this->db->get()->result();


    }


}