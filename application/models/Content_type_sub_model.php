<?php
class Content_type_sub_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("shape_content_type_sub");
    }

    public function getData($where = false, $system_language_code = false)
    {

        // $this->db->select('shape_content_type_sub.ContentTypeSubID,shape_content_type_sub.ContentTypeID,system_languages.ShortCode,
        // (SELECT type_text.Title FROM `shape_content_type_text` type_text where type_text.ContentTypeID = shape_content_type_sub.ContentTypeID and type_text.SystemLanguageID = shape_content_type_sub_text.SystemLanguageID) as content_type_title,shape_content_type_sub.ContentTypeSubFor,shape_content_type_sub_text.Title,shape_content_type_sub.SortOrder,shape_content_type_sub.Hide,shape_content_type_sub.IsActive');
        // $this->db->from('shape_content_type_sub');
        // $this->db->join('shape_content_type_sub_text', 'shape_content_type_sub.ContentTypeSubID = shape_content_type_sub.ContentTypeSubID');
        // $this->db->join('system_languages', 'shape_content_type_sub_text.SystemLanguageID = system_languages.SystemLanguageID');

        
        // if ($system_language_code) {
        //     $this->db->where('system_languages.ShortCode', $system_language_code);
        // } else {
        //     $this->db->where('system_languages.IsDefault', '1');
        // }
        // if ($where) {
        //     $this->db->where($where);
        // }
        // $this->db->where($this->table . '.Hide', '0');
        // $result = $this->db->get();
        
        $query = "SELECT shape_content_type_sub.ContentTypeSubID,shape_content_type_sub.ContentTypeID,system_languages.ShortCode,
        (SELECT type_text.Title FROM `shape_content_type_text` type_text where type_text.ContentTypeID = shape_content_type_sub.ContentTypeID and type_text.SystemLanguageID = shape_content_type_sub_text.SystemLanguageID) as content_type_title,shape_content_type_sub.ContentTypeSubFor,shape_content_type_sub_text.Title,shape_content_type_sub.SortOrder,shape_content_type_sub.Hide,shape_content_type_sub.IsActive FROM `shape_content_type_sub` 
        join shape_content_type_sub_text on shape_content_type_sub.ContentTypeSubID = shape_content_type_sub_text.ContentTypeSubID
        join system_languages on shape_content_type_sub_text.SystemLanguageID = system_languages.SystemLanguageID ";
        if($system_language_code)
        {
            $query .= "where system_languages.ShortCode = '$system_language_code' ";
        }
        else
        {
            $query .= "where system_languages.IsDefault = 1 ";
        }
        if ($where) {
            $query .= $where;
        }
        $result = getCustomRows($query);

        return $result;
    }
}
