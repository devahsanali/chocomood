<?php
Class Search_tag_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("search_tags");

    }

    public function getSearchTags()
    {
        $this->db->select('search_tags.SearchTag, count(search_tags.SearchTagID) as SearchCount');
        $this->db->from('search_tags');
        $this->db->group_by('search_tags.SearchTag');
        $this->db->order_by('SearchCount', 'DESC');
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result();
    }


}