<?php
                Class Character_model extends Base_Model
                {
                    public function __construct()
                    {
                        parent::__construct("customization_characters");

                    }

                    public function getData($where = false, $system_language_code = false)
                    {

                        $this->db->select('*');
                        $this->db->from('customization_characters');
                        
                        if ($where) {
                            $this->db->where($where);
                        }
                        $this->db->where($this->table . '.Hide', '0');
                        $result = $this->db->get();

                        // echo $this->db->last_query();exit();
                        return $result->result();
                    }

                    public function entry_update($id,$data,$update_by) {
                        $this->db->where('CharacterID', $id);
                        $this->db->update($this->table, $data);
                    }

                }