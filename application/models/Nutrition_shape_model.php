<?php
    Class Nutrition_shape_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("shape_nutritions");

        }



        public function shapeNutrition($product_id,$language){
        	$this->db->select('nutritions_text.Title,nutritions.NutritionID,shape_nutritions.Quantity');
        	$this->db->from('nutritions');
        	$this->db->join('nutritions_text','nutritions_text.NutritionID = nutritions.NutritionID');
        	$this->db->join('system_languages', 'system_languages.SystemLanguageID = nutritions_text.SystemLanguageID');
        	$this->db->join('shape_nutritions','shape_nutritions.NutritionID = nutritions.NutritionID');
        	$this->db->where('shape_nutritions.ProductID',$product_id);
            $this->db->where('system_languages.ShortCode', $language);

        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result_array();

        }


    }