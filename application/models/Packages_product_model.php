<?php
    Class Packages_product_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("product_packages");

        }



       /* public function productPackages($product_id,$language){
        	$this->db->select('packages_text.Title,packages.PackagesID,product_packages.*');
        	$this->db->from('packages');
        	$this->db->join('packages_text','packages_text.PackagesID = packages.PackagesID');
        	$this->db->join('system_languages', 'system_languages.SystemLanguageID = packages_text.SystemLanguageID');
        	$this->db->join('product_packages','product_packages.PackagesID = packages.PackagesID');
        	$this->db->where('product_packages.ProductID',$product_id);
            $this->db->where('system_languages.ShortCode', $language);

        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result_array();

        }*/

          public function productPackages($product_id,$language, $front_end = 0){
            $this->db->select('packages_text.quantity,packages_text.Title,packages.PackagesID,product_packages.*');
            $this->db->from('packages');
            $this->db->join('packages_text','packages_text.PackagesID = packages.PackagesID');
            $this->db->join('system_languages', 'system_languages.SystemLanguageID = packages_text.SystemLanguageID');
            $this->db->join('product_packages','product_packages.PackagesID = packages.PackagesID');
            $this->db->where('product_packages.ProductID',$product_id);
            $this->db->where('system_languages.ShortCode', $language);
            if($front_end == 1){
                $this->db->where('packages.IsActive',1);
            }

        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result_array();

        }


    }