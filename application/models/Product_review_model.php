<?php
Class Product_review_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("product_reviews");

    }

	
    public function getTotalReviews($ProductID)
    {
        $this->db->select('COUNT(*) as total_reviews');
        $this->db->from('product_reviews');
        $this->db->where('ProductID', $ProductID);
        $result = $this->db->get();
        return $result->row();
    }


}