<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo "Free Shipment Charges" ?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url(); ?>cms/<?= $ControllerName;?>/update" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate="">
                            <input type="hidden" name="form_type" value="update">
                            <input type="hidden" name="ShippingMethodID" value="<?php echo $result->ShippingMethodID; ?>">


                            <div class="row">
                                <div class="col-md-4 col-xs-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="SiteName"><?php echo lang('amount'); ?> Amount :</label>
                                        <input type="text" class="form-control shppng-amount" name="MaxAmountForFree" id="MaxAmountForFree" required value="<?php echo $result->MaxAmountForFree; ?>">
                                        <span class="sr-currency">S.R</span>
                                    </div>
                                </div>
                               
                            </div>

                            
                            <div class="form-group label-floating">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        <?php echo lang('submit'); ?>
                                    </button>



                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>