<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Shipments</h4>
                        <div class="toolbar">
                            <ul class="nav nav-pills">
                                <li class="active nav-pills-warning">
                                    <a href="#driver_shipments" data-toggle="tab" aria-expanded="true">To Driver</a>
                                </li>
                                <li>
                                    <a href="#api_shipments" data-toggle="tab" aria-expanded="true">To Shipment API</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="material-datatables tab-pane active" id="driver_shipments">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Driver Name</th>
                                        <th>Driver Mobile</th>
                                        <th>Customer Name</th>
                                        <th>Customer Mobile</th>
                                        <th>Order #</th>
                                        <th>Order Status</th>
                                        <th>Order Amount</th>
                                        <th>Received At</th>
                                        <th><?php echo lang('actions'); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($driver_shipments) {
                                        foreach ($driver_shipments as $order) {
                                            $status = $order->Status;
                                            if ($status == 1) {
                                                $class = "btn btn-sm";
                                            } else if ($status == 2) {
                                                $class = "btn btn-primary btn-sm";
                                            } else if ($status == 3) {
                                                $class = "btn btn-warning btn-sm";
                                            } else if ($status == 4) {
                                                $class = "btn btn-success btn-sm";
                                            } else if ($status == 5) {
                                                $class = "btn btn-danger btn-sm";
                                            }
                                            ?>
                                            <tr id="<?php echo $order->OrderID; ?>">
                                                <td>
                                                    <a href="<?php echo base_url('cms/user/edit') . '/' . $order->DriverID; ?>"
                                                       target="_blank" title="Click to view driver details">
                                                        <?php echo ucfirst($order->AssignedDriverName); ?>
                                                    </a>
                                                </td>
                                                <td><?php echo $order->AssignedDriverMobile; ?></td>
                                                <td><?php echo $order->FullName; ?></td>
                                                <td><?php echo $order->Mobile; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url() . 'cms/orders/view/' . $order->OrderID; ?>"
                                                       target="_blank"
                                                       title="Click to view order details"><?php echo $order->OrderNumber; ?></a>
                                                </td>
                                                <td>
                                                    <button class="<?php echo $class; ?>">
                                                        <?php echo $order->OrderStatusEn; ?>
                                                        <div class="ripple-container"></div>
                                                    </button>
                                                </td>
                                                <td><?php echo number_format($order->TotalAmount, 2); ?> SAR</td>
                                                <td><?php echo date('d-m-Y h:i A', strtotime($order->CreatedAt)); ?></td>
                                                <td>
                                                    <a href="<?php echo base_url('cms/orders/view/' . $order->OrderID); ?>"
                                                       class="btn btn-simple btn-warning btn-icon edit"><i
                                                                class="material-icons"
                                                                title="View order details">assignment</i>
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="material-datatables tab-pane" id="api_shipments">
                                <table id="" class="datatable table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Customer Name</th>
                                        <th>Customer Mobile</th>
                                        <th>Order #</th>
                                        <th>Order Status</th>
                                        <th>Order Amount</th>
                                        <th>Received At</th>
                                        <th><?php echo lang('actions'); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($api_shipments) {
                                        foreach ($api_shipments as $order) {
                                            $status = $order->Status;
                                            if ($status == 1) {
                                                $class = "btn btn-sm";
                                            } else if ($status == 2) {
                                                $class = "btn btn-primary btn-sm";
                                            } else if ($status == 3) {
                                                $class = "btn btn-warning btn-sm";
                                            } else if ($status == 4) {
                                                $class = "btn btn-success btn-sm";
                                            } else if ($status == 5) {
                                                $class = "btn btn-danger btn-sm";
                                            }
                                            ?>
                                            <tr id="<?php echo $order->OrderID; ?>">
                                                <td><?php echo $order->FullName; ?></td>
                                                <td><?php echo $order->Mobile; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url() . 'cms/orders/view/' . $order->OrderID; ?>"
                                                       target="_blank"
                                                       title="Click to view order details"><?php echo $order->OrderNumber; ?></a>
                                                </td>
                                                <td>
                                                    <button class="<?php echo $class; ?>">
                                                        <?php echo $order->OrderStatusEn; ?>
                                                        <div class="ripple-container"></div>
                                                    </button>
                                                </td>
                                                <td><?php echo number_format($order->TotalAmount, 2); ?> SAR</td>
                                                <td><?php echo date('d-m-Y h:i A', strtotime($order->CreatedAt)); ?></td>
                                                <td>
                                                    <a href="<?php echo base_url('cms/orders/view/' . $order->OrderID); ?>"
                                                       class="btn btn-simple btn-warning btn-icon edit"><i
                                                                class="material-icons"
                                                                title="View order details">assignment</i>
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- tab-content -->
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>
<script>
    $(document).ready(function () {
        $('table.datatable').DataTable({
            "ordering": true
        });
    });
    setTimeout(function () {
        window.location.reload();
    }, 60000);
</script>