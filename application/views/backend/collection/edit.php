<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if (!empty($languages)) {
    foreach ($languages as $key => $language) {
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        $common_fields4 = '';
        $common_fields5 = '';
        $category_dropdown = '';
        $sub_category_dropdown = '';
        $product_dropdown = '';
        $link_field  = '';
        if ($key == 0) {
           
            $collection_products = explode(',', $result[$key]->ProductID);

            /*foreach ($products as $product) {
                $product_dropdown .= '<option ' . ((in_array($product->ProductID, $collection_products)) ? 'selected="selected"' : '') . ' value="' . $product->ProductID . '">' . $product->Title . '</option>';
            }*/
            $link_field = '<div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title">Url Link</label>
                                        <input type="text" name="Link" required  class="form-control" id="Link" value="'.$result[$key]->Link.'">
                                    </div>
                                </div>';
           
            $common_fields2 = '<div class="row">
                                
                                    <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" ' . ((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '') . '/> ' . lang('is_active') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsFeatured">
                                                <input name="IsFeatured" value="1" type="checkbox" id="IsFeatured" ' . ((isset($result[$key]->IsFeatured) && $result[$key]->IsFeatured == 1) ? 'checked' : '') . '/> ' . lang('is_featured') . '
                                            </label>
                                        </div>
                                    </div>
                                </div>-->
                                </div>';
             if(file_exists($result[$key]->HomeImage)){
                $common_fields3 .= '<div class="row"><div class="col-md-3 col-sm-3 col-xs-3">';
                $common_fields3 .= ' <img src="' . base_url() . $result[$key]->HomeImage . '" style="height:200px;width:200px;">';
                $common_fields3 .= '</div></div>';

             }                  


            $common_fields3 .= '<div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                <label>Home Page Image</label>
                                    <div class="form-group">
                                        <label>&nbsp;</label> 
                                        <input type="file" name="HomeImage[]">
                                        <p>Dimission For 1st and 2nd (400*450) For 3rd (400*950) and For 4th(800*450)</p>
                                    </div>
                                </div>
                            </div><hr>' ;                   

            
            /*$collection_images = get_images($result[$key]->CollectionID, 'collection');
            $common_fields4 = '<div class="row">';
            if ($collection_images) {
                foreach ($collection_images as $collection_image) {
                    $common_fields4 .= '<div class="col-md-3 col-sm-3 col-xs-3"><i class="fa fa-trash delete_image" data-image-id="'.$collection_image->SiteImageID.'" aria-hidden="true"></i>
                <img src="' . base_url() . $collection_image->ImageName . '" style="height:200px;width:200px;">
            </div>';
                }
            }
            $common_fields4 .= '<div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Image</label>
                                                    <input type="file" name="Image[]" multiple="multiple">
                                                    <p>'.lang('cannot_upload_more_then').'</p>
                                                </div>
                                            </div>
                                        </div>';
            $common_fields5 = '<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Choose ' . lang('products') . '</label>
                                        <select name="ProductID[]" class="form-control" id="ProductID" required multiple>
                                           
                                            ' . $product_dropdown . '
                                        </select>
                                    </div>
                                </div>
                                
                            </div>';*/
        }

        $lang_tabs .= '<li class="' . ($key == 0 ? 'active' : '') . '">
                                        <a href="#' . $language->SystemLanguageTitle . '" data-toggle="tab">
                                            ' . $language->SystemLanguageTitle . '
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane ' . ($key == 0 ? 'active' : '') . '" id="' . $language->SystemLanguageTitle . '">
                      <form action="' . base_url() . 'cms/' . $ControllerName . '/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="' . base64_encode($language->SystemLanguageID) . '">
                                                    <input type="hidden" name="' . $TableKey . '" value="' . base64_encode($result[0]->$TableKey) . '">
                                                    <input type="hidden" name="IsDefault" value="' . $language->IsDefault . '">

                                                   
                                                    <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title' . $key . '">' . lang('title') . '</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title' . $key . '" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                               
                                                            </div>
                                                        </div>
                                                        '.$link_field.'
                                                        
                                                         
                                                    </div>
                                                    


                                                    <!--<div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Description">' . lang('description') . '</label>
                                                                <textarea class="form-control" name="Description" id="Description" style="height: 100px;">' . ((isset($result[$key]->Description)) ? $result[$key]->Description : '') . '</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Ingredients">' . lang('ingredients') . '</label>
                                                                <textarea class="form-control" name="Ingredients" id="Ingredients" style="height: 100px;">' . ((isset($result[$key]->Ingredients)) ? $result[$key]->Ingredients : '') . '</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Specifications">Specifications</label>
                                                                <textarea class="form-control summernote" name="Specifications" id="Specifications" style="height: 100px;">' . ((isset($result[$key]->Specifications)) ? $result[$key]->Specifications : '') . '</textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Tags">' . lang('tags') . '</label>
                                                                <input type="text" name="Tags" required  class="form-control" id="Tags" value="' . ((isset($result[$key]->Tags)) ? $result[$key]->Tags : '') . '">
                                                            </div>
                                                        </div>
                                                         <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Keywords">' . lang('keywords') . '</label>
                                                                <input type="text" name="Keywords" required  class="form-control" id="Keywords"  value="' . ((isset($result[$key]->Keywords)) ? $result[$key]->Keywords : '') . '">
                                                            </div>
                                                        </div>
                                                    </div>
                                                     <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label" for="MetaTages">'.lang('meta_tages').'</label>
                                                                    <input type="text" name="MetaTags" required  class="form-control" id="MetaTages" value="'.((isset($result[$key]->MetaTags)) ? $result[$key]->MetaTags : '').'">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label" for="MetaKeywords">'.lang('meta_keywords').'</label>
                                                                    <input type="text" name="MetaKeywords" required  class="form-control" id="MetaKeywords" value="' . ((isset($result[$key]->MetaKeywords)) ? $result[$key]->MetaKeywords : '') . '">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label" for="MetaDescription">'.lang('meta_description').'</label>
                                                                    <textarea class="form-control" name="MetaDescription" id="MetaDescription" style="height: 100px;">' . ((isset($result[$key]->MetaDescription)) ? $result[$key]->MetaDescription : '') . '</textarea>
                                                                </div>
                                                            </div>
                                                        </div>-->


                                                    ' . $common_fields2 . '

                                                    ' . $common_fields3 . '
                                                   
                                                    

                                                    
                                                   
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            ' . lang('submit') . '
                                                        </button>
                                                        <a href="' . base_url() . 'cms/' . $ControllerName . '">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         ' . lang('back') . '
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';


    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->Title; ?> </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    
    $(document).ready(function () {
        $(".delete_image").on('click',function(){
            var id=$(this).attr('data-image-id');

            url = "cms/collection/deleteImage2";
            reload = "<?php echo base_url();?>cms/collection/edit/<?php echo $result[0]->CollectionID; ?>";
            deleteRecord(id,url,reload);

        });
    });
</script>