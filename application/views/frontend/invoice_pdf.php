<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
	<title>Invoice</title>
	<style>
	    .edHeading {
		border-bottom: 2px solid black;
		margin: 0 0 10px;
		/* padding: 0 10px; */
    	}
    
    	.edHeading h3 {
    		margin: 0 10px 0 0;
    		font-size: 20px;
    		font-weight: bolder;
    		line-height: 24px;
    	}
    
    	.inv_Heading h3 {
    		margin: 0 0 25px;
    		font-size: 45px;
    		font-weight: bold;
    		line-height: 24px;
    	}
    
    	.edHeading h3 img {
    		height: 24px;
    		display: inline-block;
    		vertical-align: top;
    		margin-right: 3px;
    	}
    
    
    	.table-bordered {
    		border: 1px solid #dee2e6;
    	}
    
    	.table-bordered td,
    	.table-bordered th {
    		border: 1px solid #dee2e6;
    	}
    
    	.table {
    		border-collapse: collapse;
    		width: 100%;
    		max-width: 100%;
    		margin-bottom: 1rem;
    		background-color: transparent;
    	}
    	* {
    		font-family: Calibri;
    	}
	</style>
</head>

<body>

	<div class=" ">
		<table style="width:100%;margin-top:20px;">
			<tr>
				<td style="width:150px;vertical-align: top;">
					<img src="<?php echo base_url('assets/frontend/images/logoin.png'); ?>" style="width:90px;;">
				</td>
				<td style="vertical-align: top;width:190px">
					<table>
						<tr>
							<td>
								<h4 style="font-size: 14px;font-weight: bold;">
									BILL FROM:
								</h4>
							</td>
						</tr>
						<tr>
							<td>
								<h4 style="font-size: 14px;font-weight: bold;">
									CHOCOMOOD
								</h4>
							</td>
						</tr>
						<tr>
							<td>
								<p style="font-size: 12px;"><?php echo $order->StoreTitle; ?><br>

									<?php echo $order->StoreCityTitle; ?><br>
									Kingdom of Saudi Arabia
								</p>
							</td>
						</tr>
					</table>
				</td>
				<td bgcolor="" style="width:20px;">
					&nbsp;
				</td>
				<td style="width:150px; font-size: 20px;">
					<h4 class="reg-no" style="font-weight: normal;">VAT REG No.</h4>
					<h3 style="font-weight: normal;"><?php echo $order->VatNo; ?></h3>
				</td>
				<td style="vertical-align: top;">

					<img src="<?php echo base_url(); ?><?= $barcode; ?>">
					<!--<a href="javascript:void(0);" onclick="window.print();">-->
					<!--	<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/print.jpg" height="28" width="28">-->
					<!--</a>-->
				</td>
			</tr>
		</table>

		<table style="width:100%">
			<tr>
				<td colspan="2" class="edHeading" style="width:360px;">
					<h3 style="font-size: 14px;">BILL TO:</h3>
				</td>
				<td bgcolor="" style="width:50px;">
					&nbsp;
				</td>
				<td class="edHeading" style="width:25%; margin-right:10px">
					<h3 style="font-size: 14px;">ORDER DETAILS:</h3>
				</td>
				<td class="edHeading" style="width:25%">
				    <table>
				        <tr>
				            <td>
				            	<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/userIcon.jpg" style="width:20px">
				            </td>
				            <td width="150px">
				            	<h3 style="font-size:14px">CUSTOMER ID: <?php echo $order->UserID; ?></h3>
				            </td>
				        </tr>
				    </table>
				</td>
			</tr>
		</table>
		<table style="width:100%">
			<tr>
				<td style="vertical-align: top;width:150px">
					<table>
					    <tr>
					        <td>
					        	<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/userIcon.jpg" style="width:20px">
					        </td>
					        <td>
					        	<strong style="font-size:12px">
					        		<?php echo $order->FullName; ?>					        			
					        	</strong>
					        </td>
					    </tr>
					    <tr>
					        <td></td>
					        <td><p><?php echo $order->MobileNo; ?></p></td>
					    </tr>
					</table>
				</td>

				<td style="vertical-align: top;width:200px">
					<table>
					    <tr>
					        <td>
					        	<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/MapIcon.jpg" style="width:20px;">
					        </td>
					        <td>
					        	<strong class="haveIcon" style="display:inline-block"> Address</strong>
					        </td>
					    </tr>
					    <tr>
					        <td></td>
					        <td>
					            <p style="padding-left:50px; font-size:12px"><?php echo $order->Street; ?><br>
									<?php echo $order->BuildingNo; ?><br>
									<?php echo $order->AddressCity; ?><br>
									Kingdom of Saudi Arabia
								</p>
					        </td>
					    </tr>
					</table> 
				</td>
				<td style="width:20px;" >
					&nbsp;
				</td>

				<td style="width:170px;vertical-align: top;">
				    <table>
				        <tr>
				            <td style="width:40px;border-left: 1px solid black;border-bottom: 1px solid black; vertical-align: top;text-align:center">
				            		<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/invoiceNo.jpg" height="20px" width="20px" >
				            </td>
				            <td>
				                <table>
				                    <tr>
				                        <td width="100px"><strong style="font-size:10px">Invoice No. :</strong></td>
				                    </tr>
				                    <tr>
				                        <td>
				                        	<p style="font-size:12px"><?php echo $order->OrderNumber; ?></p>
				                        </td>
				                    </tr>
				                </table>
				            </td>
				        </tr>
				    </table>
					
					
					<table>
				        <tr>
				            <td style="width:40px;border-left: 1px solid black;border-bottom: 1px solid black; vertical-align: top;text-align:center"><img src="<?php echo base_url(); ?>assets/frontend/images/invoice/timeIcon.jpg" height="20px" width="20px" ></td>
				            <td>
				                <table>
				                    <tr>
				                        <td width="100px">
				                        	<strong style="font-size:10px">Time of issue:</strong>
				                        </td>
				                    </tr>
				                    <tr>
				                        <td>
				                        	<p style="font-size:12px;"><?php echo date('H:i a', strtotime($order->OrderCreateDate)); ?></p>
				                        </td>
				                    </tr>
				                </table>
				            </td>
				        </tr>
				    </table>
				    
					<table>
				        <tr>
				            <td style="width:40px;border-left: 1px solid black;border-bottom: 1px solid black; vertical-align: top;text-align:center"><img src="<?php echo base_url(); ?>assets/frontend/images/invoice/cardIcon.jpg" height="20px" width="20px" ></td>
				            <td>
				                <table>
				                    <tr>
				                        <td width="100px"><strong style="font-size:10px;">Payment Method:</strong></td>
				                    </tr>
				                    <tr>
				                        <td>
				                            <p style="font-size:12px;">
        				                        <?php
                        						if ($order->CollectFromStore == 1) {
                        							echo "Payment on Store";
                        						} else {
                        							echo $order->PaymentMethod;
                        						}
                        						?>
                						    </p>
                						</td>
				                    </tr>
				                </table>
				            </td>
				        </tr>
				    </table>
				    
					<table>
					    <tr>
					        <td style="width:40px;border-left: 1px solid black;border-bottom: 1px solid black; vertical-align: top;text-align:center"><img src="<?php echo base_url(); ?>assets/frontend/images/invoice/trackOrder.jpg" height="20" width="20"></td>
					        <td>
					            <table>
					                <tr>
					                    
					                   <td width="110px">
					                       <strong style="font-size:10px;">Tracking Order No.</strong>
					                   </td>
					                </tr>
					                <tr>
					                    <td><p style="font-size:12px;"><?php echo $order->OrderNumber; ?></p></td>
					                </tr>
					            </table>
					       </td>
					    </tr>
					    
					</table>

				</td>

				<td>
				    
				    <table>
					    <tr>
					        <td style="width:40px;border-left: 1px solid black;border-bottom: 1px solid black; vertical-align: top;text-align:center"><img src="<?php echo base_url(); ?>assets/frontend/images/invoice/calender.jpg" height="20" width="20"></td>
					        <td>
					            <table>
					                <tr>
					                    <td width="100px">
					                        <strong style="font-size:10px;">Date of issue:</strong>
					                    </td>
					                </tr>
					                <tr>
					                    <td>
					                        <p style="font-size:12px;"><?php echo date('d/m/Y', strtotime($order->OrderCreateDate)); ?></p>
					                    </td>
					                </tr>
					            </table>
					        </td>
					    </tr>
					</table>
					<?php
					if ($order->CollectFromStore != 1) {
					?>
					<table>
					    <tr>
					        <td style="width:40px;border-left: 1px solid black;border-bottom: 1px solid black; vertical-align: top;text-align:center">
					        		<img src="<?php echo base_url(); ?>assets/frontend/images/invoice/dropBox.jpg" height="20" width="20">
					        </td>
					        <td>
					            <table>
					                <tr>
					                    
					                   <td width="100px">
					                       <?php if ($order->AWBNumber != '') {
                						    ?>
                					        <strong style="font-size:10px;">Tracking ID:</strong>
                					        <?php } else { ?>
                					        <strong style="font-size:10px;">Estimated Delivery:</strong>
                					        <?php
                						    }
                						    ?>
					                   </td>
					                </tr>
					                <tr>
					                    <?php if ($order->AWBNumber != '') {
            						    ?>
            					        <td>
            					        	<p style="font-size:12px;"><?= @$order->AWBNumber; ?></p>
            					        </td>
            					        	<?php } else { ?>
            					        <td>
            					        	<?php
            									$site_setting = site_settings();
            									$days_to_deliver = "+$site_setting->DaysToDeliver days";
            								?>
	            							<p style="font-size:12px;">
	            								<?php echo date('d/m/Y', strtotime($days_to_deliver, strtotime($order->OrderCreateDate))); ?>
	            							</p>
            							</td>
	            					        <?php
	            						    }
	            						    ?>
					                </tr>
					            </table>
					       </td>
					    </tr>
					    
					</table>
					
					<?php
					}
                    ?>
					
					<?php if ($order->CollectFromStore == 1) { ?>
					<table>
					    <tr>
					        <td style="width:40px;border-left: 1px solid black;border-bottom: 1px solid black; vertical-align: top;text-align:center"><img src="<?php echo base_url(); ?>assets/frontend/images/invoice/pStore.jpg" height="20" width="20"></td>
					        <td>
					            <table>
					                <tr>
					                    <td>
					                        <strong style="font-size:12px;">Pick Up Store:</strong>
					                    </td>
					                </tr>
					                <tr>
					                    <td>
					                        <p style="font-size:12px;"><?php echo $order->StoreTitle; ?> / <?php echo $order->StoreAddress; ?></p>
					                    </td>
					                </tr>
					            </table>
					        </td>
					    </tr>
					</table>
					<?php
                    } ?>
	                
	                <?php if ($order->CouponCodeUsed != '') { ?>
	                
	                <table>
					    <tr>
					        <td style="width:40px;border-left: 1px solid black;border-bottom: 1px solid black; vertical-align: top;text-align:center"><img  src="<?php echo base_url(); ?>assets/frontend/images/invoice/codeUSed.jpg" height="20" width="20"></td>
					        <td>
					            <table>
					                <tr>
					                    <td>
					                        <strong style="font-size:12px;">Promo Code Used:</strong>
					                    </td>
					                </tr>
					                <tr>
					                    <td>
					                        <p style="font-size:12px;"><?php echo $order->CouponCodeUsed; ?></p>
					                    </td>
					                </tr>
					            </table>
					        </td>
					    </tr>
					</table>
				
					<?php
                    } ?>
                    
                    <?php if ($order->CollectFromStore == 0) { ?>
                    
                    <table>
					    <tr>
					        <td style="width:40px;border-left: 1px solid black;border-bottom: 1px solid black; vertical-align: top;text-align:center">
					            <?php if ($order->ShipmentMethodID == 4) { ?>
                            		<img  src="<?php echo base_url(); ?>assets/frontend/images/invoice/typeShipment.jpg" height="20" width="20">
                            	<?php
                            	} else { ?>
                            		<img  src="<?php echo base_url(); ?>assets/frontend/images/invoice/shipping.jpg" height="20" width="20">
                            	<?php
                            	} ?>
					            
					        </td>
					        <td>
					            <table>
					                <tr>
					                    <td width="100px">
					                        <strong style="font-size:10px">Type of Shipment:</strong>
					                    </td>
					                </tr>
					                <tr>
					                    <td>
					                        <p style="font-size: 10px;">
					                            <?php
                                        		if ($order->SemsaShippingAmount && $order->SemsaShippingAmount > 0) {
                                        			echo 'SMSA';
                                        		} else {
                                        			if (isset($order->CollectFromStore) && $order->CollectFromStore > 0) {
                                        				//echo "Pickup for Store";
                                        				echo "Collect From Store";
                                        			} else {
                                        				echo $order->ShipmentMethodTitle;
                                        			}
                                        		}
                                        		?>
					                        </p>
					                    </td>
					                </tr>
					            </table>
					        </td>
					    </tr>
					</table>
					
					<?php
                    } ?>

	                <table>
					    <tr>
					        <td style="width:40px;border-left: 1px solid black;border-bottom: 1px solid black; vertical-align: top;text-align:center"><img style="" src="<?php echo base_url(); ?>assets/frontend/images/invoice/status.jpg" height="20" width="20"></td>
					        <td>
					            <table>
					                <tr>
					                    <td width="100px">
					                        <strong style="font-size:10px">Status:</strong>
					                    </td>
					                </tr>
					                <tr>
					                    <td style="font-size:12px">
					                        <p ><?php echo $order->OrderStatusEn; ?></p>
					                    </td>
					                </tr>
					            </table>
					        </td>
					    </tr>
					</table>
            </td>
        </tr>
</table>

<table style="width:320px">
	<tr>
		<td colspan="2" class="edHeading">
			<h3 style="font-size: 14px;">RECIPIENT INFORMATION:</h3>
		</td>
	</tr>
	</table>
	<table>
	<tr>
		<td style="vertical-align: top;width:150px">
		    <table>
			    <tr>
			        <td><img style="width:20px" src="<?php echo base_url(); ?>assets/frontend/images/invoice/userIcon.jpg"></td>
			        <td><strong style="font-size:12px"><?php echo $order->FullName; ?></strong></td>
			    </tr>
			    <tr>
			        <td></td>
			        <td><p><?php echo $order->MobileNo; ?></p></td>
			    </tr>
			</table>

		</td>
		<td style="vertical-align: top;width:190px">
		    <table>
			    <tr>
			        <td><img style="width:20px" src="<?php echo base_url(); ?>assets/frontend/images/invoice/MapIcon.jpg"></td>
			        <td><strong>Address</strong></td>
			    </tr>
			    <tr>
			        <td></td>
			        <td><p><p style="font-size:12px"><?php echo $order->Street; ?><br>
				<?php echo $order->BuildingNo; ?><br>
				<?php echo $order->AddressCity; ?><br>
				Kingdom of Saudi Arabia</p></p></td>
			    </tr>
			</table>
		</td>
	</tr>
</table>

<!-- <div class="row mt-4 mx-3 mb-5">
	<div class="col-md-12">
		<div class="inv_Heading">
			
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="table-responsive"> -->
							<table class="table" style="border:none;margin-bottom: 5px;">
								<thead>
									<tr>
										<th class="inv_Heading" align="left" style="border:none;">
											<h3 style="font-size: 36px;">Invoice:</h3>
										</th>
									</tr>
								</thead>
							</table>
							<table class="table table-bordered">
								<thead>
									<tr>
										<th height="50px" style="padding:0 5px" class="text-center">
											<h6 style="font-weight500;">SN</h6>
										</th>
										<th height="50px" style="padding:0 5px" class="text-center">
											<h6 style="font-weight500;">SKU</h6>
										</th>
										<th height="50px" style="padding:0 5px" class="text-center">
											<h6 style="font-weight500;">Image</h6>
										</th>
										<th height="50px" style="padding:0 5px" class="text-center">
											<h6 style="font-weight500;">Title</h6>
										</th>
										<th height="50px" style="padding:0 5px" class="text-center">
											<h6 style="font-weight500;">Discription</h6>
										</th>
										<th height="50px" style="padding:0 5px" class="text-center">
											<h6 style="font-weight500;">Unit</h6>
										</th>
										<th height="50px" style="padding:0 5px" class="text-center">
											<h6 style="font-weight500;">QTY</h6>
										</th>
										<th height="50px" style="padding:0 5px" class="text-center">
											<h6 style="font-weight500;">Price</h6>
										</th>
										<th height="50px" style="padding:0 5px" class="text-center">
											<h6 style="font-weight500;">Sub Total</h6>
										</th>
										<th height="50px" style="padding:0 5px" class="text-center">
											<h6 style="font-weight500;">VAT</h6>
										</th>
										<th height="50px" style="padding:0 5px" class="text-center">
											<h6 style="font-weight500;">VAT Rate</h6>
										</th>
										<th height="50px" style="padding:0 5px" class="text-center">
											<h6 style="font-weight:500;">TOTAL</h6>
										</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i = 1;
									$total_cost = 0;
									$order_items = getOrderItems($order->OrderID);
									$taxes = getTaxShipmentCharges('Tax');
									$tax_amount = @(isset($taxes[0]->Amount)) ? $taxes[0]->Amount : 0;
									$tax_factor = @(isset($taxes[0]->Type)) ? (($taxes[0]->Type == 'Fixed') ? '' : $tax_amount . '%') : '';
									$subtotal_total = 0;
									$tax_rate_total = 0;
									foreach ($order_items as $item) {
										$total_cost = $total_cost + (($item->Amount * $item->Quantity) + get_taxt_amount($item->Amount * $item->Quantity));
									?>
										<tr>
											<td height="150px" class="text-center" style="vertical-align:top;padding:10px 5px;text-align: center;font-size:14px;"><?php echo $i; ?></td>
											<td height="150px" class="text-center" style="vertical-align:top;padding:10px 5px;text-align: center;font-size:14px;">
												<?php echo $item->SKU; ?>
											</td>
											<td height="150px" style="vertical-align:top;padding:10px 5px;text-align: center;font-size:14px;">
												<?php
												if ($item->ItemType == 'Product') { ?>
													<img src="<?php echo base_url() . '/' . get_images($item->ProductID, 'product', false); ?>" style="width: 70px;height: 70px;">
												<?php } elseif ($item->ItemType == 'Customized Shape') { ?>
													<img src="<?php echo base_url($item->CustomizedShapeImage); ?>" style="width: 70px;height: 70px;">
												<?php } else { ?>
													<img src="<?php echo front_assets("images/" . $item->ItemType . ".png"); ?>" style="width: 70px;height: 70px;">
												<?php } ?>
											</td>
											<td height="150px" class="text-center" style="vertical-align:top;padding:10px 5px;text-align: center;font-size:14px;">
												<?php echo $item->Title; ?>
											</td>

											<td height="150px" class="text-center" style="vertical-align:top;padding:10px 5px;text-align: center;font-size:14px;">
												<?php
												echo $item->Description;
												?>
											</td>
											<td height="150px" class="text-center" style="vertical-align:top;padding:10px 5px;text-align: center;font-size:14px;">
												<?php echo (($item->PriceType == 'pcs') ? ' kg' : 'pcs'); ?>
											</td>
											<td height="150px" class="text-center" style="vertical-align:top;padding:10px 5px;text-align: center;font-size:14px;">
												<?php
												if ($item->PriceType == 'kg') {
													$product_packages = get_product_packages($item->ProductID, $language);
													if ($product_packages) {
														foreach (@$product_packages as $k => $v) {
															if ($item->package_id == $v['PackagesProductID']) {
																// echo $v['Title'] . ' x ' . $item->Quantity;
																echo $item->Quantity;
															}
														}
													} else {
														echo $item->Quantity;
													}
												} else {
													echo $item->Quantity;
												}
												?>

											</td>
											<td height="150px" class="text-center" style="vertical-align:top;padding:10px 5px;text-align: center;font-size:14px;"><?php echo number_format($item->Amount, 2); ?>
												SAR
											</td>

											<td height="150px" class="text-center" style="vertical-align:top;padding:10px 5px;text-align: center;font-size:14px;">
												<?php
												$s_total = $item->Amount * $item->Quantity;
												$subtotal_total = $subtotal_total + $s_total;
												echo number_format($s_total, 2);
												?>
												SAR
											</td>
											<td height="150px" class="text-center" style="vertical-align:top;padding:10px 5px;text-align: center;font-size:14px;">


												<?php echo $tax_factor; ?>
											</td>
											<td height="150px" class="text-center" style="vertical-align:top;padding:10px 5px;text-align: center;font-size:14px;">
												<?php echo get_taxt_amount($item->Amount * $item->Quantity); ?>
												SAR
											</td>
											<td height="150px" class="text-center" style="vertical-align:top;padding:10px 5px;text-align: center;font-size:14px;">
												<?php
												$t_total =  get_taxt_amount($item->Amount * $item->Quantity);
												$tax_rate_total = $tax_rate_total + $t_total;
												$s_total = $item->Amount * $item->Quantity;

												//echo $t_total; 
												echo number_format($t_total + $s_total, 2);
												?>
												SAR
											</td>

										</tr>
									<?php $i++;
									}
									?>

									<tr>
										<td height="50px" class="text-right" colspan="8" style="text-align: right;"><strong>Total</strong></td>
										<td height="50px" class="text-center clr" style="text-align: center;font-size:14px;"><?php echo number_format($subtotal_total, 2); ?> SAR</td>
										<td height="50px" class="text-center clr" style="text-align: center;font-size:14px;"><?php echo $tax_factor; ?> </td>
										<td height="50px" class="text-center clr" style="text-align: center;font-size:14px;"><?php echo number_format($tax_rate_total, 2); ?> SAR</td>
										<td height="50px" class="text-center clr" style="text-align: center;font-size:14px;"><?php echo number_format($total_cost, 2); ?> SAR</td>
									</tr>
								</tbody>
								<tfoot>
									<?php if ($order->CollectFromStore == 0) { ?>
										<tr>
											<td height="50px" class="text-left no-borders" colspan="4" style="padding:0 5px;">
												<p>Transactions made based on gross total</p>
											</td>
											<td height="50px" class="text-right no-borders" style="text-align:right;" colspan="<?= ($order->TotalShippingCharges != 0 && !$order->SemsaShippingAmount && $order->SemsaShippingAmount < 1) ? 4 : 7 ?>">
											    <h6><strong>Shipment Charges <?= ($order->TotalShippingCharges == 0) ? '' : '(including  VAT)' ?> : &nbsp;</strong></h6>
											    <img style="width:20px;height: auto;vertical-align: bottom;" src="<?php echo base_url(); ?>assets/frontend/images/invoice/shipping.jpg" class="td-icon">
											</td>
											<td height="50px" class="text-center clr" style="text-align: center;font-size:14px;">
												<?php
												if ($order->TotalShippingCharges == 0) {
													echo freeShippingTitle();
												} else {
													echo $order->TotalShippingCharges . ' SAR';
												}
												?>
											</td>
											<?php if ($order->TotalShippingCharges != 0 && !$order->SemsaShippingAmount && $order->SemsaShippingAmount < 1) { ?>
												<td height="50px" class="text-center clr">
													<?php
													echo $tax_amount;
													?> %
												</td>
												<td height="50px" class="text-center clr">
													<?php
													echo number_format(($order->TotalShippingCharges / 100) * $tax_amount, 2);
													?> SAR
												</td>
												<td height="50px" class="text-center clr">
													<?php
													echo number_format((($order->TotalShippingCharges / 100) * $tax_amount) + $order->TotalShippingCharges, 2);
													$order->TotalAmount = number_format($order->TotalAmount + ($order->TotalShippingCharges / 100) * $tax_amount, 2);
													?> SAR
												</td>
											<?php } ?>
										</tr>
									<?php
									} ?>
									<!-- <tr>
                        		<td height="50px" class="text-right no-borders" colspan="6"><strong>Tax Paid:</strong><img src="<?php echo base_url(); ?>assets/frontend/images/invoice/cardDiscount.jpg" class="td-icon"></td>
                        		<td height="50px" class="text-center clr"><?php echo number_format($order->TotalTaxAmount, 2); ?> SAR</td>
                        	</tr> -->
									<?php
									if ($order->DiscountAvailed > 0) { ?>
										<tr>
											<td height="50px" class="text-right no-borders" colspan="11">
												<h6><strong>Discount:</strong></h6>
												<img style="width:20px;height: auto;vertical-align: bottom;" src="<?php echo base_url(); ?>assets/frontend/images/invoice/cardDiscount.jpg" class="td-icon">
											</td>
											<td height="50px" class="text-center clr">
												<?php echo number_format($order->DiscountAvailed, 2); ?> SAR
											</td>
										</tr>
									<?php
									} ?>
									<tr>
										<td height="50px" class="text-right no-borders" colspan="11" style="text-align: right;">
											<h6><strong>Grand Total : </strong></h6> 
											<img style="width:20px;height: auto;vertical-align: bottom;" src="<?php echo base_url(); ?>assets/frontend/images/invoice/grandTotal.jpg" class="td-icon">
										</td>
										<td height="50px" class="text-center clr" style="text-align: center;font-size:14px;"><?php echo $order->TotalAmount; ?> SAR
										</td>
									</tr>
								</tfoot>
							</table>
						<!-- </div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div> -->

<div class="row note">
	<div class="col-md-12">
		<h2 class="text-center" style="font-size:30px;text-align:center; font-weight: normal;">THANK YOU FOR YOUR BUSINESS!</h2>
		<?php if ($order->CollectFromStore == 1) {
			$site_setting = site_settings();
		?>
			<p style="color: red">Your order will be Cancelled if not picked up in <?= @$site_setting->DaysToDeliver ?> Days from the <?= @$order->StoreTitle; ?> store</p>
		<?php
		}
		?>
		<!--<p><b>PLEASE NOTE:</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
 </p>-->
	</div>
</div>
</div>

</body>

</html>