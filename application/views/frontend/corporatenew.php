<section class="content titlarea" style="background-image: url(<?php echo base_url(); ?>assets/frontend/images/background.jpg);  background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-12" style="border-bottom: 3px solid #d19a00;">
                <h2>Corporate Gifts</h2>
                
            </div>
            <div class="col-12 cust-ord">
                <h3 style="color: #bd9371;">
                    CORPORATE GIFTS
                    <span style="color: #fff;">FOR YOUR VALUED CLIENTS OR EMPLOYEES</span>
                </h3>
                <p style="color: #fff;">What better way to leave an impression on your employees or clients than by sending them a box of Cacao & Cardamom chocolates? Our luxury packaging creates giddy expectation, along with the wide selection offers suitable choices for all occasions. Create and customize your corporate gifting presentation however you fancy. The chocolates can be customized with your logo or may carry a message card that has your logo on it. All you need to do is provide us with your chosen assortment of chocolates and your gift list. We will take it from there and execute the task without any trouble, regardless of how many locations you need the chocolates to be sent to! Have more specific ideas? Give us a call at 281.501.3567 or email us at info@cacaoandcardamom.com with your questions, comments, concerns. Fill out the contact form below with as much detail about your custom order as possible, then upload any text, artwork, or logos associated with your request, and a representative will contact you regarding details and price.</p>
            </div>
        </div>
    </div>

    <div class="container content products" style=" border: 1px solid #c59b26; background-color: #3e2013b0; padding: 20px 20px; margin-top: 50px; margin-bottom: 50px;">
        <div class="col-md-6" style="background-image: url(<?php echo base_url(); ?>assets/frontend/images/redchoc.jpeg); min-height: 500px; background-size: cover;">
            <div class="text" style="padding: 25% 15% 20% 20%;"><h1 style="color: #fff; text-align: center; ">Gifts for your entire list</h1>
            <p class="lead" style="color: #fff; text-align: center; ">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut nisi eget lectus molestie molestie et sit amet dolor.</p>
            </div>
        </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6" style="background-image: url(<?php echo base_url(); ?>assets/frontend/images/gift1.jpg); min-height: 250px; background-size: cover;">
                     
                    </div>
                    <div class="col-md-6" style="background-image: url(<?php echo base_url(); ?>assets/frontend/images/gift2.jpg); min-height: 250px; background-size: cover;">
                      
                    </div>
                    <div class="col-md-6" style="background-image: url(<?php echo base_url(); ?>assets/frontend/images/gift3.jpg); min-height: 250px; background-size: cover;">   
                    
                    </div>
                    <div class="col-md-6" style="background-image: url(<?php echo base_url(); ?>assets/frontend/images/gift4.jpg); min-height: 250px; background-size: cover;">  
                    
                    </div>
                    
                </div>
                
            </div>
        </div>
</section>

<section style="background-image: url(<?php echo base_url(); ?>assets/frontend/images/banner-choc.jpg); background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-8">
                   <div class="card" style=" border: 1px solid #c59b26; background-color: #3e2013b0; margin: 150px 50px; padding: 5%; max-width: 700px;">
                              <h2 style="color: #fff; padding-top: 20px; ">Health Benifits</h2>
                             <p style="color: #fff;  text-align: center;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut nisi eget lectus molestie molestie et sit amet dolor. Duis consectetur mi turpis, ac interdum orci laoreet ac. Suspendisse potenti. Cras tristique ante quis ipsum porta auctor. Vivamus et augue id massa fringilla bibendum. Quisque finibus ligula nec augue cursus, ac pharetra lorem condimentum.</p> 
                </div>           
            </div>    
        </div>
    </div>    
</section>

<section style="background-image: url(<?php echo base_url(); ?>assets/frontend/images/background2.jpg);  background-size: cover;">
    <div class="container">
        <div class="row ">
            <div class="col-md-4">
                 <div class="card" style=" border: 1px solid #c59b26; background-color: #3e2013b0; margin: 150px 0px; ">
                         <img src="<?php echo base_url(); ?>assets/frontend/images/gift3.jpg" alt="chocolate" style="padding: 20px;" >
                              <h2 style="color: #fff; padding-bottom: 10px; padding-top: 20px;">Chocolate</h2>
                             <p style="color: #fff; padding: 20px 20px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut nisi eget lectus molestie molestie et sit amet dolor. Duis consectetur mi turpis, ac interdum orci laoreet ac. Suspendisse potenti.</p>
                             
                </div>  
             </div>
            
            <div class="col-md-4">
                 <div class="card" style=" border: 1px solid #c59b26; background-color: #3e2013b0; margin: 150px 0px; ">
                         <img src="<?php echo base_url(); ?>assets/frontend/images/gift1.jpg" alt="chocolate" style="padding: 20px;" >
                              <h2 style="color: #fff; padding-bottom: 10px; padding-top: 20px;">Chocolate</h2>
                             <p style="color: #fff; padding: 20px 20px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut nisi eget lectus molestie molestie et sit amet dolor. Duis consectetur mi turpis, ac interdum orci laoreet ac. Suspendisse potenti.</p>
                             
                </div>  
             </div>

             <div class="col-md-4">
                 <div class="card" style=" border: 1px solid #c59b26; background-color: #3e2013b0; margin: 150px 0px; ">
                         <img src="<?php echo base_url(); ?>assets/frontend/images/gift3.jpg" alt="chocolate" style="padding: 20px;"  >
                              <h2 style="color: #fff; padding-bottom: 10px; padding-top: 20px; ">Chocolate</h2>
                             <p style="color: #fff; padding: 20px 20px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut nisi eget lectus molestie molestie et sit amet dolor. Duis consectetur mi turpis, ac interdum orci laoreet ac. Suspendisse potenti.</p>
                             
                </div>  
             </div> 
                </div>
                
            </div>
            
        </div>
        
    </div>
</section>

<section class="text-area" style="background-image: url(<?php echo base_url(); ?>assets/frontend/images/cacao.jpg); background-size: cover;">
    <div class="container">
    <div class="row">
            <div class="col-md-5 col-md-offset-3 ">
                <div class="card" style=" border: 1px solid #c59b26; background-color: #3e2013b0; margin: 150px 0px; padding: 5%; max-width: 600px;">
                              <h2 style="color: #fff; padding-top: 30px; ">Need Big Supply</h2>
                             <p style="color: #fff;  text-align: center;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut nisi eget lectus molestie molestie et sit amet dolor. Duis consectetur mi turpis, ac interdum orci laoreet ac. Suspendisse potenti. Cras tristique ante quis ipsum porta auctor. Vivamus et augue id massa fringilla bibendum. Quisque finibus ligula nec augue cursus, ac pharetra lorem condimentum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut nisi eget lectus molestie molestie et sit amet dolor. Duis consectetur mi turpis, ac interdum orci laoreet ac. Suspendisse potenti. Cras tristique ante quis ipsum porta auctor. Vivamus et augue id massa fringilla bibendum. Quisque finibus ligula nec augue cursus, ac pharetra lorem condimentum. 
                </div>                
        </div>
            </div>    
        </div>
</section>

<style type="text/css">
    
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  max-width: 300px;
  margin: auto;
  text-align: center;
  font-family: arial;
  margin: 50px 0px;
}

.price {
  color: grey;
  font-size: 22px;
}

.card button {
  border: none;
  outline: 0;
  padding: 12px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;
}

.card button:hover {
  opacity: 0.7;
}

@media (max-width: 991px) {
   .col-md-4 {
    margin-left: 25%;
   }

   .col-md-5.col-md-offset-3 {
    margin: 60px;
   }
}

@media (max-width: 767px) {
   section.content.titlarea {
        padding:10%;
    }
    .col-md-5.col-md-offset-3 {
    margin-left: 70px;
   }

   .col-md-4 {
    margin-left: 28%;
   }
}

@media (max-width: 540px) {
    .col-md-5.col-md-offset-3 {
    margin-left: 60px;
   }

   .col-md-4 {
    margin-left: 20%;
   }
}
</style>
