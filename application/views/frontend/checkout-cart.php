<style>
small, .small {
    font-size: 13px;
    font-weight: normal;
    color: #000;
}
    .content.products .inbox i.add_wishlist_to_cart {
        position: absolute;
        top: 10px;
        right: 55px;
        bottom: auto;
        left: auto;
        color: #fb7176;
        font-size: 20px;
    }
    .content.checkout .navarea li:nth-child(1) img.gold {display:none;}
    .content.checkout .navarea li:nth-child(1) img.gray {display:inline-block;}
    .content.checkout .navarea li:nth-child(1).active img.gold {display:inline-block;}
    .content.checkout .navarea li:nth-child(1).active img.gray {display:none;}
    @media (max-width:767px) {
        .mb-xs-small {
            margin-bottom:10px !important;
        }
        .mb-xs-small h6.visible-xs {
            margin: 0 0 4px !important;
        }
        .mb-xs-small h5 {
            margin: 0 !important;
        }
    }
    .div-height {
        height: 50vh;
    }
    @media(max-width: 768px){
        .content.products .inbox i.add_wishlist_to_cart {
            position: absolute;
            top: 10px;
            right: auto !important;
            bottom: auto;
            left: 400px !important;
            color: #fb7176;
            font-size: 20px;
        }
        .content.products .inbox i.p_liked {
            position: absolute;
            top: 10px;
            right: 270px !important;
            bottom: auto;
            left: auto !important;
            color: #fb7176;
            font-size: 20px;
            -webkit-transition-duration: 0.65s;
            -o-transition-duration: 0.65s;
            transition-duration: 0.65s;
        }
    }
    @media(max-width: 425px){
        .content.products .inbox i.p_liked {
            position: absolute;
            top: 10px;
            right: 110px !important;
            bottom: auto;
            left: auto !important;
            color: #fb7176;
            font-size: 20px;
            -webkit-transition-duration: 0.65s;
            -o-transition-duration: 0.65s;
            transition-duration: 0.65s;
        }
        .content.products .inbox i.add_wishlist_to_cart {
            position: absolute;
            top: 10px;
            right: auto !important;
            bottom: auto;
            left: 230px !important;
            color: #fb7176;
            font-size: 20px;
        }
    }
    @media(max-width: 375px){
        .content.products .inbox i.p_liked {
            position: absolute;
            top: 10px;
            right: 85px !important;
            bottom: auto;
            left: auto !important;
            color: #fb7176;
            font-size: 20px;
            -webkit-transition-duration: 0.65s;
            -o-transition-duration: 0.65s;
            transition-duration: 0.65s;
        }
        .content.products .inbox i.add_wishlist_to_cart {
            position: absolute;
            top: 10px;
            right: auto !important;
            bottom: auto;
            left: 245px !important;
            color: #fb7176;
            font-size: 20px;
        }
    }
    @media(max-width: 320px){
        .content.products .inbox i.p_liked {
            position: absolute;
            top: 10px;
            right: 85px !important;
            bottom: auto;
            left: auto !important;
            color: #fb7176;
            font-size: 20px;
            -webkit-transition-duration: 0.65s;
            -o-transition-duration: 0.65s;
            transition-duration: 0.65s;
        }
        .content.products .inbox i.add_wishlist_to_cart {
            position: absolute;
            top: 10px;
            right: auto !important;
            bottom: auto;
            left: 215px !important;
            color: #fb7176;
            font-size: 20px;
        }
    }
</style>
<section class="content products checkout pt10">
    <div class="container pt-5">
        <div class="row">
            <div class="col-md-12">
                <ul class="navarea">
                    <li class="active">
                        <a data-toggle="tab" href="#shopping">
                        <img class="gray" src="<?php echo front_assets() ?>images/shopping_basket_gray_small.png">
                        <img class="gold" src="<?php echo front_assets() ?>images/shopping_basket_gold_small.png">
                            <span><?php echo lang('shopping_basket'); ?></span>
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#wishlist">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                            <span><?php echo lang('wishlist'); ?></span>
                        </a>
                    </li>
                </ul>
                <br>
                <div class="tab-content">
                    <div id="shopping" class="tab-pane fade in active">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="wbox">
                                   <div class="minWidthScroll">
                                   <div class="row">
                                        <div class="col-sm-4">
                                            <h6 class="hidden-xs"><?php echo lang('products'); ?></h6>
                                        </div>
                                        <div class="col-sm-3">
                                            <h6 class="hidden-xs text-center"><?php echo lang('quantity'); ?></h6>
                                        </div>
                                        <div class="col-sm-2">
                                            <h6 class="hidden-xs text-center"><?php echo lang('unit_price'); ?></h6>
                                        </div>
                                        <div class="col-sm-3">
                                            <h6 class="hidden-xs text-center"><?php echo lang('total'); ?></h6>
                                        </div>
                                    </div>
                                    <?php
                                    $i = 1;
                                    $total = 0;
                                    $checkoutType = 1;
                                    $productIsProductIds = [];
                                    foreach ($cart_items as $cart_item) {
                                        if($cart_item->ItemType != 'Product')
                                        {
                                            $checkoutType = 0;
                                        }
                                        else
                                        {
                                            array_push($productIsProductIds,$cart_item->ProductID);
                                        }
                                        if ($cart_item->CorporateMinQuantity > 0) {
                                            $MinQuantity = $cart_item->CorporateMinQuantity;
                                        } else {
                                            $MinQuantity = 1;
                                           
                                        }
                                        ?>
                                        <div class="row" id="TempOrderID<?php echo $cart_item->TempOrderID; ?>">
                                            <div class="col-sm-4 mb-xs-small">
                                                <h6 class="visible-xs"><?php echo lang('products'); ?></h6>
                                                <?php
                                                if ($cart_item->ItemType == 'Product') { ?>
                                                    <a class="d-flex" href="<?php echo base_url() . 'product/detail/' . productTitle($cart_item->ProductID); ?>">
                                                        <img src="<?php echo base_url(get_images($cart_item->ProductID, 'product', false)); ?>">
                                                        <h5><?php echo $cart_item->Title; ?></h5>
                                                    </a>
                                                <?php } elseif ($cart_item->ItemType == 'Choco Shape') { ?>
                                                    <a class="d-flex" href="javascript:void(0);" class="chocobox_detail"
                                                       title="Click to view whats inside"
                                                       data-url= "customize/getChocoboxDetail"
                                                       data-id="<?php echo $cart_item->TempOrderID; ?>"
                                                       data-box_id="<?php echo $cart_item->CustomizedBoxID; ?>"
                                                       data-pids="<?php echo $cart_item->CustomizedOrderProductIDs; ?>"
                                                       data-box_type="<?php echo $cart_item->ItemType; ?>"
                                                       data-ribbon="<?php echo $cart_item->Ribbon; ?>">
                                                    <img src="<?php echo base_url($cart_item->CustomizedShapeImage); ?>">
                                                    <h5>Choco Shape</h5>
                                                    </a>
                                                <?php } else { ?>
                                                    <a class="d-flex" href="javascript:void(0);" class="chocobox_detail"
                                                       title="Click to view whats inside"
                                                       data-url= "customize/getChocoboxDetail"
                                                       data-id="<?php echo $cart_item->TempOrderID; ?>"
                                                       data-box_id="<?php echo $cart_item->CustomizedBoxID; ?>"
                                                       data-pids="<?php echo $cart_item->CustomizedOrderProductIDs; ?>"
                                                       data-box_type="<?php echo $cart_item->ItemType; ?>"
                                                       data-ribbon="<?php echo $cart_item->Ribbon; ?>">
                                                        <img src="<?php echo front_assets("images/" . $cart_item->ItemType . ".png"); ?>">
                                                        <h5><?php echo $cart_item->ItemType; ?></h5>
                                                        <?php if($cart_item->ItemType == 'Choco Box') { ?>
                                                        <label><?= (@$cart_item->BoxPrintable == 1)?'Printable':'Unprintable'?></label>
                                                        <?php }?>
                                                    </a>
                                                <?php }
                                                ?>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-3 mb-xs-small inputnumber mi-qty-width">
                                                <h6 class="visible-xs"><?php echo lang('quantity'); ?></h6>
                                                <?php 
                                                $this_title = "";
                                                 $this_weight = 0;
                                                if($cart_item->PriceType == 'kg'){ ?>
                                                   
                                             <select class="Package" style="display: none">
                                                    <?php
                                                     $this_weight = 0;
                                                    $product_packages = get_product_packages($cart_item->ProductID,$language);
                                                     foreach(@$product_packages as $k => $v) {
                                                        if($cart_item->Package == $v['PackagesProductID'] ){
                                                            $this_title =  $v['Title'];
                                                            $this_weight = $v['quantity'];
                                                        }
                                                    ?>
                                                    <option <?= ($cart_item->Package == $v['PackagesProductID'] )?'selected':''?> data-weight="<?= $v['quantity']?>" data-min="<?= $v['MinimumPackage']?>" data-max="<?= $v['MaximumPackage']?>" value = "<?= $v['PackagesProductID']?>"><?= $v['Title'] ?></option>
                                                    <?php } ?>
                                                  
                                                </select>
                                                       <span class="form-control "><?= $this_title;?></span>
                                                    <?php } ?>
                                                     <input type="hidden" value="<?php echo $cart_item->Quantity; ?>" class="cart_qty" />
                                                <input id="after<?php echo $i; ?>"
                                                       class="form-control quatity_num cart_quantity update_cart "
                                                       type="number"
                                                       value="<?php echo $cart_item->Quantity; ?>"
                                                       <?php
                                                        $min = 0; 
                                                        if($cart_item->Package != 0 || $cart_item->Package != NULL){
                                                            if($cart_item->package_weight){
                                                                if($this_weight > 0){
                                                                    $min = $cart_item->MinimumPackage/$this_weight;
                                                                }else{
                                                                    $min = $cart_item->MinimumPackage; 
                                                                }
                                                                if($min < 1){
                                                                    $min = 1;
                                                                }
                                                            }else if($cart_item->MinimumPackage != ''){
                                                                $min = $cart_item->MinimumPackage;
                                                            }else{
                                                                $min = $MinQuantity;
                                                            } 

                                                        }else{
                                                            $min = $MinQuantity;
                                                        }
                                                        $max = '';
                                                        if($cart_item->Package != 0 || $cart_item->Package != NULL){
                                                            $mx = 0;
                                                            if($cart_item->package_weight){
                                                                if($this_weight > 0){
                                                                    $mx = $cart_item->MaximumPackage/$this_weight;
                                                                }else{
                                                                    $mx = $cart_item->MaximumPackage;
                                                                }
                                                            }else{
                                                                $mx = $cart_item->MaximumPackage;
                                                            }
                                                            $max = 'max="'.$mx.'"';

                                                        }
                                                       ?>
                                                       min="<?= $min ?>"
                                                       <?= $max; ?>
                                                       data-temp_order_id="<?php echo $cart_item->TempOrderID; ?>"
                                                       data-item_price="<?php echo $cart_item->TempItemPrice; ?>"  />
                                                       
                                                       <!-- <?php if($cart_item->PriceType == 'kg'){ ?>
                                                        <span><?php echo $cart_item->Quantity * 1000; ?> <?php echo ($language == 'AR' ? 'غرام' : 'Grams'); ?><br><a class="small" href="<?php echo base_url() . 'product/detail/' . productTitle($cart_item->ProductID); ?>"><?php echo lang('Add more'); ?></a></span>
                                                       <?php } ?> -->
                                            <input type='hidden' id='PriceType' value='<?= $cart_item->PriceType?>' >
                                            </div>
                                            <div class="col-sm-2 mb-xs-small">
                                                <h6 class="visible-xs"><?php echo lang('price'); ?></h6>
                                                <?php
                                                    $IsOnOffer = false;
                                                    $Price = $cart_item->TempItemPrice;
                                                    $DiscountType = $cart_item->DiscountType;
                                                    $DiscountFactor = $cart_item->Discount;
                                                    if ($DiscountType == 'percentage') {
                                                        $IsOnOffer = true;
                                                        $Discount = ($DiscountFactor / 100) * $Price;
                                                        if ($Discount > $Price) {
                                                            $ProductDiscountedPrice = 0;
                                                        } else {
                                                            $ProductDiscountedPrice = $Price - $Discount;
                                                        }
                                                    } elseif ($DiscountType == 'per item') {
                                                        $IsOnOffer = true;
                                                        $Discount = $DiscountFactor;
                                                        if ($Discount > $Price) {
                                                            $ProductDiscountedPrice = 0;
                                                        } else {
                                                            $ProductDiscountedPrice = $Price - $DiscountFactor;
                                                        }
                                                    } else {
                                                        $Discount = 0;
                                                        if ($Discount > $Price) {
                                                            $ProductDiscountedPrice = 0;
                                                        } else {
                                                            $ProductDiscountedPrice = $Price;
                                                        }
                                                    }

                                                    if($IsOnOffer)
                                                    {
                                                        ?>
                                                        <h5 style="text-decoration: line-through;"><?php echo number_format($cart_item->TempItemPrice, 2); ?> <?php echo lang('sar'); ?></h5>
                                                        <h5 ><?php echo number_format($ProductDiscountedPrice, 2); ?> <?php echo lang('sar'); ?></h5>
                                                        <?php
                                                    }
                                                    else
                                                    {
                                                        ?>
                                                        <h5><?php echo number_format($cart_item->TempItemPrice, 2); ?> <?php echo lang('sar'); ?></h5>
                                                        <?php
                                                    }
                                                ?>
                                               
                                                <?php
                                                if ($cart_item->PriceType == 'kg') {
                                                    ?>
                                                <?= ($language == 'AR')? 'كيلو  غرام/' : ''; ?>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="col-sm-3 mb-xs-small dropdown editbox">
                                                <h6 class="visible-xs"><?php echo lang('total'); ?></h6>
                                                <h5>
                                                <?php
                                                if ($cart_item->PriceType == 'kg') {
                                                    
                                                    $PerGramPrice = 0;
                                                    $Perpieceprice = 0;
                                                    $PackageQuantity = 0;
                                                    $packageTotal = 0;
                                                    foreach(@$product_packages as $k => $v) {
                                                        if($cart_item->Package == $v['PackagesProductID'] )
                                                        {
                                                            $PerGramPrice = $v['PerGramPrice'];
                                                            $Perpieceprice = $v['PerPiecePrice'];
                                                            $PackageQuantity = $v['quantity'];
                                                           // $packageTotal = ( $PerGramPrice / $Perpieceprice ) * $PackageQuantity;
                                                        }
                                                    }
                                                    $packageTotal = $cart_item->TempItemPrice;
                                                    ?>
                                                    <span id="TotalPrice_<?php echo $cart_item->TempOrderID; ?>"><?php echo number_format($ProductDiscountedPrice * $cart_item->Quantity, 2); ?></span>
                                                    <?php }else{ ?>
                                                        <span id="TotalPrice_<?php echo $cart_item->TempOrderID; ?>"><?php echo number_format($ProductDiscountedPrice * $cart_item->Quantity, 2); ?></span>
                                                    <?php } ?>
                                                    <?php echo lang('sar'); ?></h5>
                                                <!-- <a href="javascript:void(0);"
                                                   onclick="removeIt('cart/removeFromCart', 'TempOrderID', <?php echo $cart_item->TempOrderID ?>);">
                                                    <button class="btn dropdown-toggle" type="button">
                                                        <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                                    </button>
                                                </a> -->
                                                <div class="dropdown">
                                                    <button class="btn dropdown-toggle" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
                                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <?php if ($cart_item->ItemType == 'Product'){?>
                                                        <?php if(isLiked($cart_item->ProductID,'Product') == 'p_liked'){ ?>
                                                                <li><a href="javascript:void(0);" onclick="addToWishlist(<?php echo $cart_item->ProductID; ?>,'Product');"><?php echo lang('removed_from_wishlist'); ?></a></li>
                                                        <?php }else{ ?>
                                                            <li><a href="javascript:void(0);" onclick="addToWishlist(<?php echo $cart_item->ProductID; ?>,'Product');"><?php echo lang('click_to_add_to_your_wishlist'); ?></a></li>
                                                      <?php  } ?>
                                                      <?php  } ?>
                                                        <li><a href="javascript:void(0);" onclick="removeIt('cart/removeFromCart', 'TempOrderID', <?php echo $cart_item->TempOrderID ?>,'<?php echo lang("confirm_question");?>','<?php echo lang("are_you_sure_to_remove_this");?>','<?php echo lang("confirm");?>','<?php echo lang("cancel");?>');"><?php echo lang('delete'); ?></a></li>
                                                    </ul>
                                                </div>


                                            </div>
                                        </div>
                                        <?php
                                        if ($cart_item->PriceType == 'kg') 
                                        {
                                            $total += $ProductDiscountedPrice * $cart_item->Quantity;
                                        }else
                                        {
                                            $total += $ProductDiscountedPrice * $cart_item->Quantity;
                                        }
                                        
                                        $i++;
                                    }
                                    ?>
                                   </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="wbox side">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h6 class="mb-0" style="margin-bottom: 0;"><?php echo lang('total_bill'); ?></h6>
                                        </div>
                                    </div>
                                    <?php
                                    if ($this->session->userdata('order_coupon')) {
                                        $order_coupon = $this->session->userdata('order_coupon');
                                        $coupon_code = $order_coupon['CouponCode'];
                                        $coupon_discount_percentage = $order_coupon['DiscountPercentage'];
                                        $coupon_discount_availed = ($order_coupon['DiscountPercentage'] / 100) * $total;
                                        $total = $total - $coupon_discount_availed;
                                        ?>
                                        <p><span><?php echo lang('promo_applied'); ?>:</span> <strong><?php echo $coupon_code; ?></strong>
                                            <a class="btn" href="javascript:void(0);" onclick="removeCoupon();">
                                                <button class="btn"><?php echo lang('clear'); ?> <i class="fa fa-remove"
                                                                             style="color: red !important;"></i>
                                                </button>
                                            </a>
                                        </p>
                                        <p><span><?php echo lang('promo_discount');?> %:</span>
                                            <strong><?php echo $coupon_discount_percentage; ?></strong></p>
                                        <p><span><?php echo lang('promo_discount_availed'); ?>:</span>
                                            <strong><?php echo $coupon_discount_availed; ?> <?php echo lang('sar'); ?></strong></p>
                                    <?php } else { ?>
                                        <form action="<?php echo base_url('cart/applyCoupon'); ?>" method="post"
                                              class="couponApplyForm gift" id="couponForm">
                                            <i class="fa fa-gift" aria-hidden="true"></i>
                                            <span class="borderBox"><input type="text" name="CouponCode" placeholder="<?php echo lang('gift_voucher_code'); ?>"
                                                   class="form-control required"></span>
                                            <input type="submit" name="" value="<?= lang('Redeem')?>" class="btn btn-primary">
                                        </form>
                                    <?php }
                                    ?>
                                    <ol>
                                        <?php
                                        $shipping_amount = 0;
                                        $shipment_method = getTaxShipmentCharges('Shipment', true);
                                        if ($shipment_method) {
                                            $shipping_title = $shipment_method->Title;
                                            $shipping_factor = $shipment_method->Type == 'Fixed' ? number_format($shipment_method->Amount, 2) . ' SAR' : $shipment_method->Amount . '%';
                                            if ($shipment_method->Type == 'Fixed') {
                                                $shipping_amount = $shipment_method->Amount;
                                            } elseif ($shipment_method->Type == 'Percentage') {
                                                $shipping_amount = ($shipment_method->Amount / 100) * ($total);
                                            }
                                            ?>
                                            <li>
                                                <!--<span><i class="fa fa-truck"
                                                         aria-hidden="true"></i> <?php echo $shipping_title; ?></span>
                                                <strong id="ShippingAmount"><?php echo number_format($shipping_amount, 2); ?>
                                                    SAR</strong>-->
                                                    <span>
                                                        <i class="fa fa-file-text-o" aria-hidden="true"></i> 
                                                        <?php echo lang('total'); ?></span>
                                                    <strong id="ShippingAmount"><?php echo number_format($total, 2); ?>
                                                    <?php echo lang('sar'); ?></strong>
                                            </li>
                                        <?php }
                                        ?>
                                        <?php
                                        $total_tax = 0;
                                        $taxes = getTaxShipmentCharges('Tax');
                                        foreach ($taxes as $tax) {
                                            $tax_title = $tax->Title;
                                            $tax_factor = $tax->Type == 'Fixed' ? '' : $tax->Amount . '%';
                                            if ($tax->Type == 'Fixed') {
                                                $tax_amount = $tax->Amount;
                                            } elseif ($tax->Type == 'Percentage') {
                                               // $tax_amount = ($tax->Amount / 100) * ($total + $shipping_amount);
                                                 $tax_amount = ($tax->Amount / 100) * ($total);
                                            }
                                            ?>
                                            <li>
                                                <span>
                                                    <!-- <i class="fa fa-file-text-o"
                                                         aria-hidden="true"></i> -->
                                                         <svg class="svg-inline--fa fa-file-text-o fa-w-16" aria-hidden="true" focusable="false" data-prefix="fa" data-icon="file-text-o" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><g><path fill="currentColor" d="M156.5,447.7l-12.6,29.5c-18.7-9.5-35.9-21.2-51.5-34.9l22.7-22.7C127.6,430.5,141.5,440,156.5,447.7z M40.6,272H8.5 c1.4,21.2,5.4,41.7,11.7,61.1L50,321.2C45.1,305.5,41.8,289,40.6,272z M40.6,240c1.4-18.8,5.2-37,11.1-54.1l-29.5-12.6 C14.7,194.3,10,216.7,8.5,240H40.6z M64.3,156.5c7.8-14.9,17.2-28.8,28.1-41.5L69.7,92.3c-13.7,15.6-25.5,32.8-34.9,51.5 L64.3,156.5z M397,419.6c-13.9,12-29.4,22.3-46.1,30.4l11.9,29.8c20.7-9.9,39.8-22.6,56.9-37.6L397,419.6z M115,92.4 c13.9-12,29.4-22.3,46.1-30.4l-11.9-29.8c-20.7,9.9-39.8,22.6-56.8,37.6L115,92.4z M447.7,355.5c-7.8,14.9-17.2,28.8-28.1,41.5 l22.7,22.7c13.7-15.6,25.5-32.9,34.9-51.5L447.7,355.5z M471.4,272c-1.4,18.8-5.2,37-11.1,54.1l29.5,12.6 c7.5-21.1,12.2-43.5,13.6-66.8H471.4z M321.2,462c-15.7,5-32.2,8.2-49.2,9.4v32.1c21.2-1.4,41.7-5.4,61.1-11.7L321.2,462z M240,471.4c-18.8-1.4-37-5.2-54.1-11.1l-12.6,29.5c21.1,7.5,43.5,12.2,66.8,13.6V471.4z M462,190.8c5,15.7,8.2,32.2,9.4,49.2h32.1 c-1.4-21.2-5.4-41.7-11.7-61.1L462,190.8z M92.4,397c-12-13.9-22.3-29.4-30.4-46.1l-29.8,11.9c9.9,20.7,22.6,39.8,37.6,56.9 L92.4,397z M272,40.6c18.8,1.4,36.9,5.2,54.1,11.1l12.6-29.5C317.7,14.7,295.3,10,272,8.5V40.6z M190.8,50 c15.7-5,32.2-8.2,49.2-9.4V8.5c-21.2,1.4-41.7,5.4-61.1,11.7L190.8,50z M442.3,92.3L419.6,115c12,13.9,22.3,29.4,30.5,46.1 l29.8-11.9C470,128.5,457.3,109.4,442.3,92.3z M397,92.4l22.7-22.7c-15.6-13.7-32.8-25.5-51.5-34.9l-12.6,29.5 C370.4,72.1,384.4,81.5,397,92.4z"></path><circle fill="currentColor" cx="256" cy="364" r="28"><animate attributeType="XML" repeatCount="indefinite" dur="2s" attributeName="r" values="28;14;28;28;14;28;"></animate><animate attributeType="XML" repeatCount="indefinite" dur="2s" attributeName="opacity" values="1;0;1;1;0;1;"></animate></circle><path fill="currentColor" opacity="1" d="M263.7,312h-16c-6.6,0-12-5.4-12-12c0-71,77.4-63.9,77.4-107.8c0-20-17.8-40.2-57.4-40.2c-29.1,0-44.3,9.6-59.2,28.7 c-3.9,5-11.1,6-16.2,2.4l-13.1-9.2c-5.6-3.9-6.9-11.8-2.6-17.2c21.2-27.2,46.4-44.7,91.2-44.7c52.3,0,97.4,29.8,97.4,80.2 c0,67.6-77.4,63.5-77.4,107.8C275.7,306.6,270.3,312,263.7,312z"><animate attributeType="XML" repeatCount="indefinite" dur="2s" attributeName="opacity" values="1;0;0;0;0;1;"></animate></path><path fill="currentColor" opacity="0" d="M232.5,134.5l7,168c0.3,6.4,5.6,11.5,12,11.5h9c6.4,0,11.7-5.1,12-11.5l7-168c0.3-6.8-5.2-12.5-12-12.5h-23 C237.7,122,232.2,127.7,232.5,134.5z"><animate attributeType="XML" repeatCount="indefinite" dur="2s" attributeName="opacity" values="0;0;1;1;0;0;"></animate></path></g></svg>
                                                          <?php echo $tax_title; ?> <?php echo $tax_factor; ?></span>
                                                <strong id="TaxAmount"><?php echo number_format($tax_amount, 2); ?>
                                                <?php echo lang('sar'); ?></strong>
                                            </li>
                                            <?php
                                            $total_tax += $tax_amount;
                                        }

                                       // $total = $total + $shipping_amount + $total_tax;
                                        $total = $total + $total_tax;
                                        ?>
                                        <li><h5><span><?php echo lang('Grand_Total'); ?></span>
                                                <?php echo number_format($total, 2); ?><strong><?php echo lang('sar'); ?></strong></h5>
                                        </li>
                                    </ol>
                                    <a href="javascript:void(0);" onclick="<?= $checkoutType == 1? 'proceedToCheckout();':'proceedToCheckoutForCustomize(\''.implode(',',$productIsProductIds).'\');'?>">
                                        <button type="button" class="btn btn-primary checkoutbtn"><?php echo lang('proceed_to_checkout1'); ?>
                                        </button>
                                    </a>
                                    <p class="text-center"><a href="<?php echo base_url('product'); ?>"><?php echo lang('continue_shopping'); ?></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="wishlist" class="tab-pane fade">
                        <div class="row">
                            <?php
                             if(empty($wishlist_items))
                            {
                                ?>
                                <div style="height:50vh;">
                                    <div class="col-md-12 text-center">
                                        <p><?= lang('no_item_in_wishlist')?></p>
                                        <img src="<?php echo front_assets("images/" . "no-orders.png"); ?>" alt="no-orders" width="200px" height="300px">
                                        <p class="no-orders-text">
                                        <br>
                                        <a href="<?php echo base_url('product'); ?>?q=elegant-s40" class="btn btn-primary"> <?= lang('start_browsing_our_products')?></a>
                                        </p> 
                                    </div>
                                <div>
                                <?php
                            }
                            foreach ($wishlist_items as $wishlist_item) {
                                
                                // print_rm($wishlist_item);
                                $default_package = 0;
                                $product_packages = (object)get_product_packages($wishlist_item->ItemID,'EN');
                                foreach(@$product_packages as $k => $v) {
                                    if(@$v['DefaultPackagesID'] == @$v['PackagesID'])
                                    {
                                        $default_package = @$v['PackagesProductID'];
                                    }
                                }
                                if ($wishlist_item->ItemType == 'kg' || $wishlist_item->ItemType == 'Product') {
                                    $url = base_url() . 'product/detail/' . productTitle($wishlist_item->ItemID);
                                    $image = base_url(get_images($wishlist_item->ItemID, 'product', false));
                                } elseif ($wishlist_item->ItemType == 'Collection') {
                                    $url = base_url() . 'collection/detail/' . collectionTitle($wishlist_item->ItemID);
                                    $image = base_url(get_images($wishlist_item->ItemID, 'collection', false));
                                }
                                $offer_product = checkProductIsInAnyOffer($wishlist_item->ItemID);
                                $DiscountType = '';
                                $DiscountFactor = '';
                                if(!empty($offer_product)){
                                    
                                    $DiscountType = $offer_product['DiscountType'];
                                    $DiscountFactor = $offer_product['Discount'];
                                }
                                // print_rm($image);
                                ?>
                                <div class="col-md-3">
                                    <div class="inbox">
                                        <div class="imgbox">
                                            <img style="opacity: 1; display: block;" src="<?php echo @$image; ?>">
                                        </div>
                                        <a href="<?php echo @$url; ?>">
                                            <h4><?php echo $wishlist_item->Title; ?></h4>
                                            <h5><strong><?php echo $wishlist_item->Price; ?></strong> <?php echo lang('sar'); ?></h5>
                                        </a>
                                        <a title="<?php echo lang('click_to_add_to_your_wishlist'); ?>" href="javascript:void(0);"
                                        onclick="addWishlistToCart(<?php echo $wishlist_item->ItemID; ?>, '<?php echo ucfirst($wishlist_item->ItemType); ?>', '<?php echo $wishlist_item->Price; ?>','<?php echo $wishlist_item->IsCorporateProduct; ?>','<?php echo $DiscountType; ?>','<?php echo $DiscountFactor; ?>','<?= $default_package?>','<?= $wishlist_item->PriceType ?>');"><i
                                                    class="fa fa-heart <?php echo isLiked($wishlist_item->ItemID, $wishlist_item->ItemType); ?>"
                                                    id="item<?php echo $wishlist_item->ItemID; ?>"
                                                    aria-hidden="true"></i></a>
                                        <a href="javascript:void(0);" class="<?= ($wishlist_item->OutOfStock == 1)? 'disable_event' : ''; ?>"  title="<?php echo lang('click_to_add_this_to_your_cart'); ?>"
                                        onclick="addWishlistToCart(<?php echo $wishlist_item->ItemID; ?>, '<?php echo ucfirst($wishlist_item->ItemType); ?>', '<?php echo $wishlist_item->Price; ?>','<?php echo $wishlist_item->IsCorporateProduct; ?>','<?php echo $DiscountType; ?>','<?php echo $DiscountFactor; ?>','<?= $default_package?>','<?= $wishlist_item->PriceType ?>');">
                                            <i class="fa fa-cart-plus add_wishlist_to_cart"></i>
                                        </a>
                                    </div>
                                </div>
                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<input type="hidden" value="<?= lang('confirm')?>" id="confirm_lang" placeholder="">
<input type="hidden" value="<?= lang('clear_this_coupon')?>" id="clear_this_coupon_lang" placeholder="">
<script>
    $('.Package').attr("style", "pointer-events: none; display:none;");


    $(document).on('change', '.Package', function () {
        var min = $( ".Package option:selected" ).data("min");
        var max = $( ".Package option:selected" ).data("max");
        var weight = $( ".Package option:selected" ).data("weight");

            min = min/weight;
            max = max/weight;

        $(".cart_quantity").val(min);
        $(".cart_quantity").prop('min',min);
        $(".cart_quantity").prop('max',max);
       
    });
    $(document).on('keypress', ".quatity_num, .discount_amount, .rate_num", function(e) {
        var x = e.which || e.keycode;
      if((x>=48 && x<=57)){
        return true;
      }else{
          e.preventDefault();
            //console.log(false);
        return false;
      }
    });

</script>
