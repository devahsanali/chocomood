<style>
.content.checkout.address .wbox {margin-bottom:30px;}
.mb-0 {margin-bottom:0px !important;}
.h-100 {height:100%;}
.editbox.alwaysBeThere .bbox {background-color: #D3E6CF !important;}
.content.checkout.address .wbox .bbox {border-radius: 5px;}/** margin-bottom:34px !important; **/
.content.checkout .wbox .row.no-border {border:0px; margin-bottom:0px;padding-bottom:0px}
.content.checkout .wbox.side ol.chkOutAdd li {margin: 0;color: #bd9371;text-align: center;}
.content.checkout .wbox.side ol.chkOutAdd li span {
    float: none;
    color: inherit;
    display: inline-block;
    vertical-align: middle;
    font-size: 18px;
    margin: 0;
    font-weight:normal;
}
.content.checkout .wbox.side ol.chkOutAdd li strong {
    font-weight: normal;
    font-size: 18px;
}
.content.checkout.address .wbox .btn.ccheckout {margin-top:0px;}
label.customradio.default {
    font-weight: 900;
    padding: <?php echo($lang == 'AR' ? ' 0px 35px 0px 0px ' : ' 0px 0px 0px 35px'); ?>;
    border-width: initial;
    border-style: none;
    border-color: initial;
    border-image: initial;
    line-height: 25px; */
}
label.customradio.default .checkmark {
    border:1px solid rgb(153,153,153);
}
.shipMethodTxt h5 strong {
    display: block;
    font-size: 18px;
    font-weight: bold;
}
.shipMethodTxt h5 {
    line-height: 1.1 !important;
}
.plusNewBox a {
    display: BLOCK;
    width: 100%;
    height: 100%;
    border: 1px dashed #86A57F;
    border-radius: 5px;
    background-color: #fff;
    position: relative;
    font-size: 0;
}
.plusNewBox a:before {
    content: '';
    width: 100px;
    height: 100px;
    background: #CE8D8D 0% 0% no-repeat padding-box;
    box-shadow: 0px 3px 6px #00000029;
    display: block;
    border-radius: 50%;
    margin: auto;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
}
.plusNewBox a:after {
    background: transparent;
    content: '+';
    width: 40px;
    height: 40px;
    display: block;
    margin: auto;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    font-size: 65px;
    color: #fff;
    text-shadow: 0px 3px 6px #00000029;
    text-align: center;
    line-height: 36px;
}
.customradio .checkmark:after {
    top: 7.5px;
    left: 8px;
}
.content.checkout.address .wbox .bbox h5 {
    font-size: 18px;
    margin: 0;
    line-height: 1.5;
    font-weight: normal;
    margin: 10px 0 8px;
    word-break: break-word;
}
.content.checkout.address .wbox .edImgBox {
    display: block;
    width: 100%;
    padding: 15px 0;
}
.content.checkout.address .wbox .edImgBox img {
    max-width: 100%;
    height: auto;
}
.content.checkout.address .wbox .bbox h5 a {
    color: #0000004E;
    margin-top: 4px;
    display: inline-block;
}
.collOptRow_f_Wrap > div {
    margin-bottom: 25px;
}
.collOptRow_f_Wrap {
    flex-wrap: wrap;
    margin-bottom: -25px !important;
}
.col-md-4.editbox.plusNewBox:nth-child(n+4) {
    display: none;
}
.mod-cnt {
    padding: 2% 6%;
    border-radius: 5px;
  }
  .modW2 {
    max-width: 1104px;
    max-height: 1176px;
}
  .mod_hdd {
    border: none;
  }
  .mod_ftt {
    border: none;
  }
  
.chcimg {
  width: 60px;
  height: 60px;
  border-radius: 17px;
}
.title {
  margin-bottom: 5%;
  margin-top: 3%;
}
h3.scnd-title {
    color: #50456D !important;
    font-size: 30px !important;
    position: relative;
    bottom: 14px;
}
h1.hed-title {
    color: #CE8D8D;
    font-size: 36px;
}  
.txt-detail p.chc-nam {
    color: #000000 !important;
    font-size: 17px !important;
}
.crs-btn.btn-dark {
    background-color: #CE8D8D;
    width: 35px;
    height: 35px;
    border-radius: 50px;
    border: none;
}
button.btn-light.crt-btn {
    width: 100%;
    height: 46px;
    background-color: #F4EBD3 !important;
    color: #000;
    border: none;
    box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.161);
    border-radius: 7px;
    font-size: 16px;
    margin-bottom: 30px;
    margin-top: 7%;
}
.cat-choc .txt-price {
    /* position: relative; */
    display: block;
    width: 100%;
    padding-right: 40px;
}
.cat-choc .txt-price p,
.cat-choc .txt-detail p.chc-nam {
    color: #000 !important;
}
.cat-choc .txt-price p span.green,
.cat-choc .txt-detail p.chc-nam span.green {color: #21AD00 !important;}
.cat-choc .txt-price p span.red,
.cat-choc .txt-detail p.chc-nam span.red {color: #C30000 !important;-webkit-text-decoration-line: line-through; /* Safari */text-decoration-line: line-through; }
.cat-choc .txt-price button.crs-btn.btn-dark {
    position: absolute;
    right: 0;
    top: 0;
    bottom: 0;
    margin: auto;
    box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.161);
    color: #fff;
    text-shadow: 0px 3px 6px #00000029;
}
.btn-info.pay-btn {
    background-color: #CE8D8D;
    color: #fff;
    width: 100%;
    height: 46px;
    box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.161);
    border-radius: 7px;
    font-size: 16px;
    border: none;
    display: inline-block;
    line-height: 46px;
}
.cat-choc {
    border-bottom: 1px solid #E2E2E2;
    padding: 14px 0;
    margin: 0 15px;
}
.chc-nam {
  font-size: 18px;
}
.txt-price {
  font-size: 18px;
}
.clk-con {
    font-size: 16px;
    color: #1A1526;
    font-weight: normal;
    margin-top: 14px;
    margin-bottom: 15px;
    padding: 0;
}
.edt-crt {
  width: 100%;
}
.edt-pay {
  width: 100%;
}
</style>
<section class="content checkout address">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Shopping <span>payment Method</span></h2>
            </div>
            <div class="col-md-12">
                <div class="wbox mb-3">
                    <div class="row">
                        <div class="col-md-12">
                            <h6>Payment Method</h6>
                            <!-- <div class="dropdown barownButton">
                                <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></button>
                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                    <li><a href="<?php echo base_url(); ?>/address/add">Add an Address</a></li>
                                </ul>
                            </div> -->
                        </div>
                    </div>
                    <div class="row no-border d-flex align-items-stretch collOptRow_f_Wrap">
                        <div class="col-md-6  editbox alwaysBeThere">
                            <div class="bbox dropdown mb-0 h-100">
                                <a href="<?php echo base_url();?><?= @$cod ?>">
                                <label class="customradio default">COD
                                   <!--  <input type="radio" name="collectionOption"
                                           class="collectFromStore" data-district-id="0" value="cod"> -->
                                    <!-- <span class="checkmark"></span> -->
                                </label>
                                <div class="edImgBox">
                                    <img src="<?php echo front_assets(); ?>images/COD.png" height="" width="" />
                                </div>
                                </a>
                                <!-- <h5>No Shipping Charges <br>Required for Pickup</h5> -->
                            </div>
                        </div>

                        <div class="col-md-6 editbox">
                            <div class="bbox dropdown mb-0 h-100">
                                <a href="<?php echo base_url();?><?= @$paytab ?>">
                                <label class="customradio default">PayTab
                                    <!-- <input type="radio" name="collectionOption" class="addressCheckbox chk"
                                           value="paytab"> -->
                                   <!--  <span class="checkmark"></span> -->
                                </label>
                                <div class="edImgBox">
                                <img src="<?php echo front_assets(); ?>images/PayTab.png" height="" width="" />
                                 </div>
                                </a>
                              
                               
                            </div>
                        </div>
                  
                        <!--  <div class="col-md-4 editbox plusNewBox">
                           <a href="<?php echo base_url(); ?>/address/add">+</a>
                                                </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function(){
            $('.collectFromStore').on('click',function(){
                $('#collect').show();
                //$('.shipment_method').hide();
                $("input:radio[name='shipment_method']").each(function(i) {
                       this.checked = false;
                });
               // $("#shipment_method").css("pointer-events","none");
                //$('#delivery').hide();
                $('#shipment_method').hide();
                
                
            });
            $('.addressCheckbox').on('click',function(){
                
                $('#collect').hide();
               // $('.shipment_method').show();
               //$("#shipment_method").css("pointer-events","auto");
                 $('#delivery').show();
                $('#shipment_method').show();
            });
            
    });
  
</script>