<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('User_model');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
        $this->data['Child_model']    = ucfirst($this->router->fetch_class()).'_text_model';
        $this->data['TableKey'] = 'UserID';
        $this->data['Table'] = 'users';
    }

    public function index()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $this->data['all_customers'] = $this->User_model->getUsers("users.RoleID = 5", $this->language);
        $this->data['online_customers'] = $this->User_model->getUsers("users.RoleID = 5 AND users.OnlineStatus = 'Online'", $this->language);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'delete':
                $this->delete();
                break;

        }
    }

    private function delete()
    {
        if (!checkUserRightAccess(71, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);
        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }


}