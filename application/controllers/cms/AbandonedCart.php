<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AbandonedCart extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->Model('Temp_order_model');
        $this->data['language'] = $this->language;
    }


    public function index()
    {
        $this->data['abandoned_carts'] = $this->Temp_order_model->getAbandonedCart();
       // print_rm($this->data['abandoned_carts']);
        $this->data['abandoned_carts_anonymous'] = $this->Temp_order_model->getAbandonedCartAnonymous();
        $this->data['view'] = 'backend/abandoned_cart/manage';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function sendAbandonedCartNotificationToUser()
    {
        $email_sent = false;
        $UserID = $this->input->post('UserID');
        $abandoned_carts = $this->Temp_order_model->getAbandonedCart();
        if (count($abandoned_carts) > 0) {
            foreach ($abandoned_carts as $abandoned_cart) {
                if ($abandoned_cart->UserID == $UserID && $abandoned_cart->Email != '') {
                    $data['to'] = $abandoned_cart->Email;
                    $data['subject'] = 'Abandoned cart at chocomood';
                    $message = "Dear " . $abandoned_cart->FullName . ",<br>You have " . $abandoned_cart->CartQuantityCount . " items in your cart. Please login and place your order at<br>" . base_url();
                    $data['message'] = email_format($message);
                    $email_sent = sendEmail($data);
                    break;
                }
            }
        }
        $response['status'] = true;
        if ($email_sent) {
            $response['message'] = 'User notified via email successfully.';
        } else {
            $response['message'] = 'Something went wrong while notifying user.';
        }
        echo json_encode($response);
        exit();
    }

}