<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('Order_model');
        $this->data['language'] = $this->language;


    }

    public function index()
    {
        $this->data['invoices'] = $this->Order_model->getOrders();
        $this->data['view'] = 'backend/invoice/manage';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function custom_order_invoice()
    {
        if (!checkUserRightAccess(107, $this->session->userdata['admin']['UserID'], 'CanView')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $this->data['invoices'] = $this->Order_model->getOrders("orders.isCustomize= 1");
        $this->data['view']     = 'backend/invoice/manage';
        $this->load->view('backend/layouts/default', $this->data);
    }
    
    public function download($OrderID)
    {
        $order_html = get_order_invoice($OrderID,'invoice',1);
        generate_pdf($order_html, $OrderID,1);
    }

}